# import needed libraries
import os.path

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web

from tornado.options import define, options

import numpy as np
import pandas as pd

import pickle
import hashlib

import dominate
from dominate.tags import *

from cryptography.fernet import Fernet

# define the port
define("port", default=8000, help="run on the given port", type=int)

# define global variables
global loggedIn
loggedIn = 0

# create classes 
# login screen
class LoginHandler(tornado.web.RequestHandler):
	def get(self):
		global username_in, password_in
		global loggedIn
		loggedIn = 0
		username_in = ' '
		password_in = ' '
		self.render('loginPage.html')

# post login screen
class PostLoginHandler(tornado.web.RequestHandler):
	def post(self):
		global loggedIn

		# check if the user hasn't logged in yet, need to grab username & password
		# otherwise use username and password already submitted
		if loggedIn==0:
			global username_in, password_in
			username_in = self.get_argument('username')
			password_in = self.get_argument('password')
			# hash password
			password_in = password_in.encode('utf-8')
			password_in = hashlib.sha256(password_in).hexdigest()
		else:
			grantAcc = self.checkUserAccessButtons()
			deleteReqUser = self.checkDelReqUserButton()
			self.checkAddRecipeButton()
			self.checkDelCurrUserButton()

			# print(grantAcc)

		# load user group
		df = pickle.load(open( "saveData.p","rb") )

		reqUserTable= self.createReqUserTable()
		currentUserTable = self.createCurrentUserTable()
		recipeTable = self.createRecipeTable()

		# need to check that login was successful
		login_success=df[df['username']==username_in]['password'].to_numpy()==password_in
		if login_success:
			role = df[df['username']==username_in]['role'][0]
			isAdmin = role =='administrator'
		else:
			role = 'no role'
			isAdmin = role =='administrator'	

		if login_success & isAdmin:
			loggedIn = 1
			self.render('admin.html', user=df[df['username']==username_in]['name'][0],
			 ps=password_in,reqUserTable=reqUserTable,currentUserTable=currentUserTable,
			 recipeTable=recipeTable)
		else:
			self.render('wrongLogin.html')

	def createReqUserTable(self):
		# load the list of requested users to import into this form
		reqUserDf = pickle.load(open( "newSaveData.p","rb") )
		reqUserDf = reqUserDf.drop('password',1)

		reqUserTable = table(border=1)
		with reqUserTable:
			with thead():
				l = tr()
				with l:
					th(' ')
					th('Name')
					th('Username')
					th('Role')
					
			with tbody():
				for i in range(0,len(reqUserDf)):
					l =tr()
					with l:
						st = "check"
						td(input_(type='checkbox',name=st+str(i),value=1,form="newUsers"))
						td(reqUserDf['name'].to_numpy()[i])
						td(reqUserDf['username'].to_numpy()[i])
						with td():
							ops = "option"
							with select(name=ops+str(i),id=ops+str(i)):
								opts = ["user","administrator"]
								option(opts[0],value="user")
								option(opts[1],value="administrator")

		return(reqUserTable)


	def createCurrentUserTable(self):
		# load the list of requested users to import into this form
		df = pickle.load(open( "saveData.p","rb") )
		df = df.drop('password',1)

		currUserTab = table(border=1)
		with currUserTab:
			with thead():
				l = tr()
				with l:
					th(' ')
					th('Name')
					th('Username')
					th('Role')
					
			with tbody():
				for i in range(0,len(df)):
					l =tr()
					with l:
						st = "box"
						td(input_(type='checkbox',name=st+str(i),value=1,form="currUsers"))
						td(df['name'].to_numpy()[i])
						td(df['username'].to_numpy()[i])
						td(df['role'].to_numpy()[i])
						
		return(currUserTab)


	def createRecipeTable(self):
		df = pickle.load(open("recipeData.p","rb"))

		recipeTable = table(border=1)
		with recipeTable:
			with thead():
				l = tr()
				with l:
					th('Name')
					th('Notes')
					
			with tbody():
				for i in range(0,len(df)):
					l =tr()
					with l:
						td(self.decryptFernet(df['Name'].to_numpy()[i]))
						td(self.decryptFernet(df['Notes'].to_numpy()[i]))

		return(recipeTable)

	def load_key(self):
		return open("key.key","rb").read()

	def encryptFernet(self,plain_text):
		shared_key = self.load_key()
		f = Fernet(shared_key)
		plain_bytes = plain_text.encode('utf-8')
		cipherToken = f.encrypt(plain_bytes)
		return(cipherToken)

	def decryptFernet(self,cipherToken):
		shared_key = self.load_key()
		f = Fernet(shared_key)
		plain_bytes = f.decrypt(cipherToken)
		plain_text = plain_bytes.decode('utf-8')
		return(plain_text)

	def getRole(self,idx):
		roles = []
		st = "option"
		# for i in range(0,len(idx)):
		roles.append(self.get_argument(st+str(int(idx[0]))))

		return(roles)


	def checkUserAccessButtons(self):
		# check if grant access button was checked
		try:
			grantAcc = self.get_argument('grantAcc')
			grantAcc = 1

			reqUserDf = pickle.load(open( "newSaveData.p","rb") )

			# find the selected users
			reqUserList = self.findSelUsers_AccessTable(len(reqUserDf),"check")

			# roles = self.get_argument("option1")

			# get the roles of the selected users
			# roles = self.getRole(reqUserList)

			reqUserDf = reqUserDf.reset_index(drop=True)
			newReqUserDf = reqUserDf.drop(reqUserList)
			pickle.dump(newReqUserDf, open( "newSaveData.p", "wb" ) )

			reqUserDf = reqUserDf.loc[reqUserList]
			reqUserDf['role'] = roles

			# load user group
			df = pickle.load(open( "saveData.p","rb") )
			df = pd.concat([df,reqUserDf])

			pickle.dump(df, open( "saveData.p", "wb" ) )

		except:
			roles = 0

		return(roles)

	def checkDelReqUserButton(self):
		# check if button was checked to delete selected users from user request table
		try:
			deleteReqUser = self.get_argument('deleteReqUser')
			deleteReqUser = 1

			reqUserDf = pickle.load(open( "newSaveData.p","rb") )

			deleteReqUserList = self.findSelUsers_AccessTable(len(reqUserDf),"check")

			reqUserDf = reqUserDf.reset_index(drop=True)
			reqUserDf = reqUserDf.drop(deleteReqUserList)
			pickle.dump(reqUserDf, open( "newSaveData.p", "wb" ) )

		except:
			deleteReqUser = 0

		return(deleteReqUser)

	def checkDelCurrUserButton(self):
		# check if button was checked to delete selected users from user request table
		try:
			deleteReqUser = self.get_argument('deleteCurrUser')
			deleteReqUser = 1

			userDf = pickle.load(open( "saveData.p","rb") )

			deleteUserList = self.findSelUsers_AccessTable(len(userDf),"box")

			userDf = userDf.reset_index(drop=True)
			userDf = userDf.drop(deleteUserList)
			pickle.dump(userDf, open( "saveData.p", "wb" ) )

		except:
			deleteReqUser = 0

		return(deleteReqUser)

	def findSelUsers_AccessTable(self,reqUserTableLen,st):
		reqUserTable = np.empty(reqUserTableLen)
		reqUserTable[:] = np.NaN
		for i in range(0,reqUserTableLen):
			try:
				val = self.get_argument(st+str(i))
				reqUserTable[i] = i
			except:
				value = 0

		reqUserTable = reqUserTable[~np.isnan(reqUserTable)]

		return(reqUserTable)


	def checkAddRecipeButton(self):
		try:
			recipeEnter = self.get_argument('recipeEnter')
			recipeEnter = 1
			self.getRecipeNameNotes()

		except:
			recipeName = ''
			recipeNotes = ''
			recipeEnter = 0


	def getRecipeNameNotes(self):
		recipeName = self.get_argument('recipeName')
		recipeNotes = self.get_argument('recipeNotes')
		# load recipe list
		df = pickle.load(open( "recipeData.p","rb") )
		# append with encrypted data
		df = df.append(pd.DataFrame([[self.encryptFernet(recipeName),self.encryptFernet(recipeNotes)]],columns=['Name','Notes']))
		pickle.dump(df, open( "recipeData.p", "wb" ) )

class newUserHandler(tornado.web.RequestHandler):
	def get(self):
		self.render('newUserReq.html')

class newUserConfirmationHandler(tornado.web.RequestHandler):
	def post(self):
		# get information
		name_in = self.get_argument('name_new')
		username_in = self.get_argument('username_new')
		password_in = self.get_argument('password_new')

		# hash password
		password_in = password_in.encode('utf-8')
		password_in = hashlib.sha256(password_in).hexdigest()

		# load requested user list
		df = pickle.load(open( "newSaveData.p","rb") )
		# append with information given
		df = df.append(pd.DataFrame([[name_in,username_in,password_in,'user']],columns=['name','username','password','role']))
		pickle.dump(df, open( "newSaveData.p", "wb" ) )

		self.write('Your information has been accepted and will be sent to the system administrator for confirmation.')


if __name__ == '__main__':
	tornado.options.parse_command_line()

	app = tornado.web.Application(
		handlers=[(r"/", LoginHandler),
		(r"/mainPage",PostLoginHandler),
		(r"/newUserRequest", newUserHandler),
		(r"/newUserConfirmation",newUserConfirmationHandler)],
		template_path=os.path.join(os.path.dirname(__file__), "templates"),
		static_path = os.path.join(os.path.dirname(__file__),"static")
	)


	http_server = tornado.httpserver.HTTPServer(app)
	http_server.listen(options.port)
	tornado.ioloop.IOLoop.instance().start()


