
import pandas as pd
import pickle
from cryptography.fernet import Fernet
import hashlib


name = 'Test'
notes = 'This is a placeholder recipe.'


# shared_key = Fernet.generate_key()

# print(shared_key)

# with open("key.key","wb") as key_file:
# 	key_file.write(shared_key)

def load_key():
	return open("key.key","rb").read()

def encryptFernet(plain_text):
	shared_key = load_key()
	f = Fernet(shared_key)
	plain_bytes = plain_text.encode('utf-8')
	cipherToken = f.encrypt(plain_bytes)
	return(cipherToken)

def decryptFernet(cipherToken):
	shared_key = load_key()
	f = Fernet(shared_key)
	plain_bytes = f.decrypt(cipherToken)
	plain_text = plain_bytes.decode('utf-8')
	return(plain_text)



df=pd.DataFrame([[encryptFernet(name),encryptFernet(notes)]],columns=['Name','Notes'])

print('Encrypted')
print(df['Name'][0])
print(df['Notes'][0])
print('Decrypted')
print(decryptFernet(df['Name'][0]))
print(decryptFernet(df['Notes'][0]))

pickle.dump(df, open( "recipeData.p", "wb" ) )