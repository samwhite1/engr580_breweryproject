# import needed libraries
import os.path

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web

from tornado.options import define, options

import numpy as np
import pandas as pd

import pickle
import hashlib

# define the port
define("port", default=8000, help="run on the given port", type=int)

# create classes 
# login screen
class LoginHandler(tornado.web.RequestHandler):
	def get(self):
		self.render('loginPage.html')

# post login screen
class PostLoginHandler(tornado.web.RequestHandler):
	def post(self):
		username_in = self.get_argument('username')
		password_in = self.get_argument('password')

		# hash password
		password_in = password_in.encode('utf-8')
		password_in = hashlib.sha256(password_in).hexdigest()

		# load user group
		df = pickle.load(open( "saveData.p","rb") )

		login_success=df[df['username']==username_in]['password'].to_numpy()==password_in

		role = df[df['username']==username_in]['role'][0]
		isAdmin = role =='administrator'

		if login_success & isAdmin:
			self.render('admin.html', user=df[df['username']==username_in]['name'][0], ps=password_in)
		else:
			# self.write('Incorrect username or password')
			self.render('wrongLogin.html')

class addUserHandler(tornado.web.RequestHandler):
	def get(self):
		self.write('Add user page')

if __name__ == '__main__':
	tornado.options.parse_command_line()
	app = tornado.web.Application(
		handlers=[(r"/", LoginHandler),(r"/mainPage",PostLoginHandler),
		(r"/addUser", addUserHandler)],
		template_path=os.path.join(os.path.dirname(__file__), "templates"),
		static_path = os.path.join(os.path.dirname(__file__),"static")
	)


	http_server = tornado.httpserver.HTTPServer(app)
	http_server.listen(options.port)
	tornado.ioloop.IOLoop.instance().start()


